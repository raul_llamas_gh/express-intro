# USO DE EXPRESS CON NODEJS


## INTRODUCCIÓN

>Dentro del conjunto de soluciones que utilizamos -tanto
PERN como MERN-, ExpressJS es el framework para
aplicaciones web que se acopla mejor para trabajar bajo
el ambiente de NodeJS.

> ExpressJS ofrece un conjunto de herramientas y
características que facilitan el desarrollo de
componentes que se necesiten ejecutar del lado del
servidor, ya sean crear servicios tanto de backend como
de middleware.


## ROUTING

> La funcionalidad que nos permite determinar cómo
nuestra aplicación (backend o middleware) va a
responder a las peticiones que un cliente realice se
le conoce como Routing. Nuestro servidor expondrá
sus servicios a modo de endpoints, los cuales son
puntos de interacción en formato URI (Uniform
Resource Identifier) que se acompañan de un
método de petición HTTP (GET, POST, PUT, etc.).

> Ejemplo:
```javascript
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.BACKENDPORT || 3030
var cors = require('cors');

app.use(cors());
app.use(express.static(__dirname));
app.use(express.static("public"));

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
  response.json({ info: 'Back End Ready (v.1.05.13)' })
})

/**
* Greetings from back end server.
* @name get/greetings
* @param {json} request Body with Name.
* @returns {json} Message with greetings.
*/
app.get('/greetings', function(request, response) {
  const name = parseInt(request.params.Name)
  if (!name) { throw 'Name is required (Back End)'; }
  return response.status(200).json({'greetings': 'Hello, ${name}!'})
})


app.listen(port, () => {
  console.log(`Back End running on port ${port}.`)
})

app.get('*', (request, response) => {
  response.json({ info: 'Request not Found at Back End' })
})

```

> Referencias:
* [Bases con Routing en NodeJS](http://expressjs.com/en/starter/basic-routing.html)
* [Tópicos avanzados con Routing en NodeJS](http://expressjs.com/en/guide/routing.html)


## DESEMPEÑO

> Cada funcionalidad necesita manejar sus errores
y no continuar la ejecución del código restante.

> No permitir el uso de [console.log] ni de
[console.err] en ambiente de producción. Para
efectos de registro en archivos .log utilícese
paquetería adecuada como Winston o Bunyan.

> Todas las funciones y métodos utilizados deberán
ser utilizados de manera asíncrona.

> Referencias:
* [Manejo de errores con NodeJS](https://www.joyent.com/node-js/production/design/errors)
* [Uso de Winston y de Bunyan](https://strongloop.com/strongblog/compare-node-js-logging-winston-bunyan)


## SINCRONÍA
> Siempre se debe considerar el uso de funcionalidades asíncronas.

> La excepción que admitimos es con funciones middleware que usen funcionalidades de backend en un orden específico y racionado. En este caso la función de middleware deberá ser expuesta como asíncrona

> Ejemplo:
```javascript
// *************** //
// *** index.js ***//
// *************** //

const express = require('express');
const bodyParser = require('body-parser');
const interfaceData = require('./transfer');
const app = express();
var cors = require('cors');

app.use(cors());
app.use(express.static(__dirname));
app.use(express.static("public"));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

/**
* Updates Candidate's Final Statement.
* {@link #putqualifyCandidate}
* @name put/candidatestatement
* @param {json} request Body with Id for Candidate and Statement.
* @returns {json} Status from storage in database and Id_Candidate.
* @example curl --data "id_candidate=888888&statement='Denied'" --header "Content-Type: application/x-www-form-urlencoded" -X PUT http://localhost:5050/candidateStatement
*/
app.route('/candidateStatement').put(async function(req, res) {
  try {
    var resultPut = await interfaceData.putCandidateStatement(req, res);
    return res.status( 201 ).send( resultPut );
    // return res.status( resultPut.statusCode ).send( resultPut.data );
  } catch(err) {
    return res.status( 400 ).send( err );
  }
});

```

```javascript
// ****************** //
// *** transfer.js ***//
// ****************** //

var reqCandidateLala = {
  id_candidate: 0,
  id_treatment: 0,
  statement: 0,
  validateTreatmentByCandidate: function () {
    return Request({
      uri: hosting + ':' + portal + '/validateCandidate',
      method: 'GET',
      form: {
        'id_candidate': reqCandidateLala.id_candidate
      },
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'x-access-token': reqCandidateLala.token
      }
    }).then(function (resTreatment) {
      if ( JSON.parse(resTreatment)[0].Id_Treatment !== undefined ) {
        reqCandidateLala.id_treatment = JSON.parse(resTreatment)[0].Id_Treatment;
      }
      return reqCandidateLala.id_treatment;
    }).then(function (idTreatment) {
      if (idTreatment === 0) {
        throw 'Id_Treatment was not Added for Candidate';
      }
      return idTreatment;
    })
  },
  updateStatementCandidate: function () {
    return Request({
      uri: hosting + ':' + portal + '/qualifyCandidate',
      method: 'PUT',
      form: {
        'id_candidate': reqCandidateLala.id_candidate,
        'id_treatment': reqCandidateLala.id_treatment,
        'statement': reqCandidateLala.statement
      },
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'x-access-token': reqCandidateLala.token
      }
    }).then(function (resCandidate) {
      if ( JSON.parse(resCandidate).Id_Candidate !== undefined ) {
        reqCandidateLala.id_candidate = JSON.parse(resCandidate).Id_Candidate;
      }
      return reqCandidateLala.id_candidate;
    }).then(function (idCandidate) {
      if (idCandidate === 0) {
        throw 'Candidate Statement was not Updated';
      }
      return idCandidate;
    })
  }
};

/**
* Updates Candidate's Statement.
* This function uses [exports] for exposure.
* @private
* @param {json} request Body with Candidate Response information: id_candidate, statement.
* @returns {string} Status from database storage.
*/
async function putCandidateStatement (request, result)  {
  const { id_candidate, statement } = request.body;
  reqCandidateLala.token = request.headers['x-access-token'];
  if (!id_candidate) { throw 'Id_Candidate is required (Middleware)'; }
  else if (!statement) { throw 'Statement is required (Middleware)'}
  else if (!reqCandidateLala.token) { throw 'Session is required (Middleware)'}
  reqCandidateLala.id_candidate = id_candidate;
  reqCandidateLala.statement = statement;
  return reqCandidateLala.validateTreatmentByCandidate()
    .then(reqCandidateLala.updateStatementCandidate)
    .catch(function (error) {
      return result.status(400).send(error);
    });
};

module.exports = {
  putCandidateStatement
};

```

## REFERENCIAS
* [ExpressJS](http://expressjs.com/)

